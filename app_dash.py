import dash
from dash import dcc, html
from dash.dependencies import Input, Output, State
import requests
import pandas as pd
import folium
from folium.plugins import MarkerCluster
from questionnaire_to_bdd import app as questionnaire_app
from rgpd import anonymize_user

app = dash.Dash(__name__, suppress_callback_exceptions=True)
server = app.server


def get_restaurants():
    response = requests.get("http://127.0.0.1:8000/restaurants")
    data = pd.DataFrame(response.json())
    unique_restaurants = data.drop_duplicates(subset=['id'])
    return unique_restaurants


def get_types():
    response = requests.get("http://127.0.0.1:8000/types")
    return response.json()


def get_restaurants_by_type(type_id):
    response = requests.get(f"http://127.0.0.1:8000/restaurants/by_type/{type_id}")
    data = pd.DataFrame(response.json())
    unique_restaurants = data.drop_duplicates(subset=['id'])
    return unique_restaurants


def get_restaurant_names():
    response = requests.get("http://127.0.0.1:8000/restaurant_names")
    return response.json()


def get_restaurant_details(restaurant_id):
    response = requests.get(f"http://127.0.0.1:8000/restaurant/{restaurant_id}")
    return response.json()


def create_map(restaurants, center=None):
    if center is None:
        center = [43.604652, 1.444209]
    folium_map = folium.Map(location=center, zoom_start=12)
    marker_cluster = MarkerCluster().add_to(folium_map)
    for idx, row in restaurants.iterrows():
        folium.Marker(location=[row['latitude'], row['longitude']],
                      popup=row['nom'],
                      tooltip=row['nom']).add_to(marker_cluster)
    return folium_map._repr_html_()


app.layout = html.Div([
    dcc.Location(id='url', refresh=False),
    html.Div(id='page-content')
])

index_page = html.Div([
    html.Div([
        html.H2("Recherche de Restaurants"),
        dcc.Input(id='search-input', type='text', placeholder="Rechercher un restaurant"),
        dcc.Dropdown(
            id='autocomplete-dropdown',
            options=[],
            multi=False,
            placeholder="Sélectionnez un restaurant"
        ),
        html.H2("Type de Restaurants"),
        dcc.Dropdown(
            id='type-dropdown',
            options=[{'label': type['type'], 'value': type['id']} for type in get_types()],
            placeholder="Sélectionnez un type de restaurant"
        ),
        html.H2("Liste des Restaurants"),
        dcc.Loading(
            id="loading-1",
            type="default",
            children=html.Div(id='restaurant-list', style={'height': '600px', 'overflowY': 'scroll'})
        )
    ], style={'width': '50%', 'display': 'inline-block', 'verticalAlign': 'top'}),
    html.Div([
        dcc.Link('Remplir le questionnaire', href='/questionnaire'),
        html.Br(),
        dcc.Link('Supprimer compte', href='/delete_account'),
        html.H2("Carte des Restaurants"),
        html.Iframe(id='map', width='100%', height='315'),
        html.Div(id='restaurant-details')
    ], style={'width': '50%', 'display': 'inline-block', 'verticalAlign': 'top'})
])

delete_account_page = html.Div([
    html.H2("Supprimer Compte"),
    dcc.Input(id='email-input', type='email', placeholder="Entrez votre adresse email"),
    html.Button('Supprimer', id='delete-button'),
    html.Div(id='delete-output')
])


@app.callback(
    Output('delete-output', 'children'),
    [Input('delete-button', 'n_clicks')],
    [State('email-input', 'value')]
)
def delete_account(n_clicks, email):
    if n_clicks is None or not email:
        return ""
    try:
        result = anonymize_user(email)
        return html.Div(f"Compte anonymisé: {result['message']}")
    except Exception as e:
        return html.Div(f"Erreur: {str(e)}", style={'color': 'red'})


@app.callback(
    Output('autocomplete-dropdown', 'options'),
    [Input('search-input', 'value')]
)
def update_autocomplete_options(search_value):
    if not search_value:
        return []
    all_names = get_restaurant_names()
    filtered_names = [name for name in all_names if search_value.lower() in name.lower()]
    return [{'label': name, 'value': name} for name in filtered_names]


@app.callback(
    Output('map', 'srcDoc'),
    [Input('type-dropdown', 'value'),
     Input('autocomplete-dropdown', 'value'),
     Input({'type': 'restaurant-item', 'index': dash.dependencies.ALL}, 'n_clicks')]
)
def update_map(selected_type, search_value, n_clicks):
    ctx = dash.callback_context
    if search_value:
        restaurants = get_restaurants()
        restaurants = restaurants[restaurants['nom'] == search_value]
    elif selected_type:
        restaurants = get_restaurants_by_type(selected_type)
    else:
        restaurants = get_restaurants()

    if not ctx.triggered:
        folium_map = create_map(restaurants)
        return folium_map

    triggered_id = ctx.triggered[0]['prop_id'].split('.')[0]

    if 'index' in triggered_id:
        restaurant_id = eval(triggered_id)['index']
        restaurant = restaurants[restaurants['id'] == restaurant_id].iloc[0]
        center = [restaurant['latitude'], restaurant['longitude']]
        folium_map = create_map(restaurants, center=center)
    else:
        folium_map = create_map(restaurants)

    return folium_map


@app.callback(
    [Output('restaurant-list', 'children')],
    [Input('type-dropdown', 'value'),
     Input('autocomplete-dropdown', 'value')]
)
def update_restaurant_list(selected_type, search_value):
    if search_value:
        restaurants = get_restaurants()
        restaurants = restaurants[restaurants['nom'] == search_value]
    elif selected_type:
        restaurants = get_restaurants_by_type(selected_type)
    else:
        restaurants = get_restaurants()

    restaurant_list = html.Ul(
        [html.Li(restaurant['nom'], id={'type': 'restaurant-item', 'index': restaurant['id']}) for _, restaurant in
         restaurants.iterrows()]
    )
    return [restaurant_list]


@app.callback(
    Output('restaurant-details', 'children'),
    [Input({'type': 'restaurant-item', 'index': dash.dependencies.ALL}, 'n_clicks')]
)
def display_restaurant_details(n_clicks):
    ctx = dash.callback_context
    if not ctx.triggered:
        return html.Div()
    triggered_id = ctx.triggered[0]['prop_id'].split('.')[0]
    if 'index' in triggered_id:
        restaurant_id = eval(triggered_id)['index']
        details = get_restaurant_details(restaurant_id)
        return html.Div([
            html.H3(details['nom']),
            html.P(f"Adresse: {details['adresse']}"),
            html.P(f"Note moyenne: {round(details['note_moyenne'], 1)} / 5"),
            html.P(f"Accessibilité PMR: {round(details['pmr_percentage'], 1)}%"),
            html.P(f"Accessibilité visuelle: {round(details['visuel_percentage'], 1)}%"),
            html.P(f"Accessibilité auditive: {round(details['auditif_percentage'], 1)}%"),
            html.H4("Commentaires:"),
            html.Ul([html.Li(f"{comment['nom_pseudo']}: {comment['commentaire']}") for comment in details['comments']])
        ])
    return html.Div()


@app.callback(Output('page-content', 'children'),
              [Input('url', 'pathname')])
def display_page(pathname):
    if pathname == '/questionnaire':
        return questionnaire_app.layout
    if pathname == '/delete_account':
        return delete_account_page
    return index_page


if __name__ == '__main__':
    app.run_server(debug=True)
