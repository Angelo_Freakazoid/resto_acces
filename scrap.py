import requests
from bs4 import BeautifulSoup
import csv
import re
import json


def fetch_soup(url):
    """ Fetch the webpage at the given URL and return a BeautifulSoup object. """
    response = requests.get(url)
    if response.status_code == 200:
        return BeautifulSoup(response.text, 'html.parser')
    else:
        return None


def find_links(soup):
    """ Extract links using a CSS selector from the BeautifulSoup object. """
    selector = ".facetwp-template > article > div > h3 > a:nth-child(1)"
    return [item.get("href") for item in soup.select(selector)]


def extract_info(url):
    """ Extract restaurant information based on the given URL. """
    soup = fetch_soup(url)
    if soup:
        name = soup.select_one('.contact__name').text.strip() if soup.select_one('.contact__name') else 'Non disponible'
        address = soup.select_one('.contact__adress').text.strip() if soup.select_one('.contact__adress') else 'Non disponible'
        city_info = soup.select_one('.contact__city').text.strip() if soup.select_one('.contact__city') else 'Non disponible'
        type_restaurant = soup.select_one('.main.gradient-text').text.strip() if soup.select_one('.main.gradient-text') else 'Autre'

        scripts = soup.find_all('script')
        latitude, longitude = None, None
        for script in scripts:
            if script.string:
                try:
                    clean_script = re.sub(r'^[^{]*', '', script.string.strip(), flags=re.DOTALL)
                    clean_script = re.sub(r'[^}]*$', '', clean_script, flags=re.DOTALL)
                    potential_data = json.loads(clean_script)

                    if 'latitude' in potential_data and 'longitude' in potential_data:
                        latitude, longitude = potential_data['latitude'], potential_data['longitude']
                        break
                except json.JSONDecodeError:
                    continue

        geopoint = f"{latitude},{longitude}" if latitude and longitude else "Non disponible"
        return [name, address, type_restaurant, city_info, geopoint]
    return ['Non disponible', 'Non disponible', 'Autre', 'Non disponible', 'Non disponible']


def main():
    base_url = "https://www.toulouse-tourisme.com/manger-boire-un-verre/restaurants-toulouse/"
    links = []
    soup = fetch_soup(base_url)

    if soup:
        current_links = find_links(soup)
        if not current_links:
            raise ValueError("Mauvais sélecteur CSS ou page sans liens")
        links.extend(current_links)

        page_number = 2
        while True:
            next_page_url = f"{base_url}?_paged={page_number}"
            soup = fetch_soup(next_page_url)
            if not soup:
                print("Erreur lors de l'accès à la page:", next_page_url)
                break

            current_links = find_links(soup)
            if not current_links:
                print("Fin des pages ou sélecteur sans résultats.")
                break

            links.extend(current_links)
            page_number += 1
            print(f"Scraping: {next_page_url}")

        with open('restaurants_toulouse.csv', 'w', newline='', encoding='utf-8') as file:
            writer = csv.writer(file)
            writer.writerow(['Nom_restaurant', 'Adresse_restaurant', 'Type_restaurant', 'Code_postal_Ville_restaurant',
                             'Geopoint_restaurant'])

            for link in links:
                info = extract_info(link)
                writer.writerow(info)
                print(f"Écriture des données pour {link}")

    else:
        raise ConnectionError("Impossible de se connecter au site web principal.")

    print(f"Nombre total de liens scrapés : {len(links)}")


if __name__ == "__main__":
    main()
