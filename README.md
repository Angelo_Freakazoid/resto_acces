# resto_acces

# resto_acces

## Pré-requis

Avant d'exécuter le projet, vous devez créer deux fichiers d'accès : `access.py` et `access.sql`.

### Fichier `access.py`

Créez un fichier nommé `access.py` et ajoutez le code suivant :

```python
import mysql.connector

def get_db_connection():
    return mysql.connector.connect(
        host='localhost',
        user='your_user',
        password='your_password',
        database='restoacces'
    )
```
Remplacez 'your_user' et 'your_password' par vos propres informations de connexion.

### Fichier `access.sql`

Créez un fichier nommé `access.sql` et ajoutez le code suivant :

```SQL
DROP USER IF EXISTS 'your_user'@'localhost';
CREATE USER 'your_user'@'localhost' IDENTIFIED BY 'your_password';
GRANT ALL PRIVILEGES ON restoacces.* TO 'your_user'@'localhost';
```
Remplacez 'your_user' et 'your_password' par vos propres informations de connexion.

## Installation et exécution

   ```bash
   # Clonez le dépôt Git :
   git clone https://gitlab.com/Angelo_Freakazoid/resto_acces

   # Accédez au répertoire du projet :
   cd resto_acces

   # Exécutez la commande suivante dans le terminal :
   chmod +x run_project.sh

   # Lancez le script en utilisant la commande suivante :
   ./run_project.sh
   ```

