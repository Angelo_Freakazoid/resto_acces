import requests


def anonymize_user(email: str):
    response = requests.delete(f"http://127.0.0.1:8000/user/delete/{email}")
    if response.status_code == 200:
        return response.json()
    else:
        raise Exception(response.json().get('detail', 'Unknown error occurred'))
