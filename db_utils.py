from access import get_db_connection
import pandas as pd


def read_restaurants():
    conn = get_db_connection()
    query = "SELECT * FROM restaurant"
    restaurants = pd.read_sql(query, conn)
    conn.close()
    return restaurants
