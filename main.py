# uvicorn main:app --reload
import uuid
from fastapi import FastAPI, HTTPException
from typing import List
import pandas as pd
from access import get_db_connection
from schemas import Restaurant, Type, RestaurantDetail, RestaurantLocation, MessageResponse

app = FastAPI()


@app.get("/restaurants/", response_model=List[Restaurant])
def read_restaurants():
    conn = get_db_connection()
    data = pd.read_sql("""
        SELECT r.* 
        FROM restaurant r
        """, conn)
    conn.close()
    return data.to_dict(orient='records')


@app.get("/restaurants/locations/", response_model=List[RestaurantLocation])
def read_restaurant_locations():
    conn = get_db_connection()
    data = pd.read_sql("SELECT id, nom, latitude, longitude FROM restaurant", conn)
    conn.close()
    return data.to_dict(orient='records')


@app.get("/types/", response_model=List[Type])
def read_types():
    conn = get_db_connection()
    data = pd.read_sql("SELECT * FROM types", conn)
    conn.close()
    return data.to_dict(orient='records')


@app.get("/restaurants/by_type/{type_id}", response_model=List[Restaurant])
def read_restaurants_by_type(type_id: int):
    conn = get_db_connection()
    query = """
        SELECT r.*, t.type AS type_name FROM restaurant r
        JOIN types_restaurant tr ON r.id = tr.restaurant_id
        JOIN types t ON tr.type_id = t.id
        WHERE tr.type_id = %s
    """
    data = pd.read_sql(query, conn, params=(type_id,))
    conn.close()
    return data.to_dict(orient='records')


@app.get("/restaurant_names", response_model=List[str])
def read_restaurant_names():
    conn = get_db_connection()
    data = pd.read_sql("SELECT nom FROM restaurant", conn)
    conn.close()
    return data['nom'].tolist()


@app.get("/restaurant/{restaurant_id}", response_model=RestaurantDetail)
def read_restaurant_details(restaurant_id: int):
    conn = get_db_connection()
    query = """
        SELECT r.nom, r.adresse, 
               AVG(a.note) as note_moyenne,
               SUM(CASE WHEN a.type_PMR = 'oui' THEN 1 ELSE 0 END) / COUNT(*) * 100 AS pmr_percentage,
               SUM(CASE WHEN a.type_visuel = 'oui' THEN 1 ELSE 0 END) / COUNT(*) * 100 AS visuel_percentage,
               SUM(CASE WHEN a.type_auditif = 'oui' THEN 1 ELSE 0 END) / COUNT(*) * 100 AS auditif_percentage
        FROM restaurant r
        LEFT JOIN avis a ON r.id = a.restaurant_id
        WHERE r.id = %s
        GROUP BY r.id
    """
    restaurant_details = pd.read_sql(query, conn, params=(restaurant_id,)).to_dict(orient='records')[0]

    query_comments = """
        SELECT u.nom_pseudo, a.commentaire
        FROM avis a
        JOIN utilisateur u ON a.utilisateur_id = u.id
        WHERE a.restaurant_id = %s
    """
    comments = pd.read_sql(query_comments, conn, params=(restaurant_id,)).to_dict(orient='records')
    conn.close()

    restaurant_details['comments'] = comments
    return restaurant_details


@app.delete("/user/delete/{email}", response_model=MessageResponse)
def delete_user(email: str):
    conn = get_db_connection()
    cursor = conn.cursor()

    try:
        cursor.execute("SELECT id FROM utilisateur WHERE email = %s", (email,))
        user = cursor.fetchone()
        if user is None:
            raise HTTPException(status_code=404, detail="User not found")

        user_id = user[0]

        unique_pseudo = f"anonymous_{uuid.uuid4()}"

        cursor.execute("""
            UPDATE utilisateur 
            SET email = 'anonymous@anonymous.fr', nom_pseudo = %s 
            WHERE id = %s
        """, (unique_pseudo, user_id))

        conn.commit()
        return {"message": "User anonymized successfully"}

    except Exception as e:
        conn.rollback()
        raise HTTPException(status_code=500, detail=f"An error occurred while deleting the user: {str(e)}")

    finally:
        conn.close()

# http://127.0.0.1:8000/openapi.json
# http://127.0.0.1:8000/docs#
