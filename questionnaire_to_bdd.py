import dash
from dash import dcc, html, callback
from dash.dependencies import Input, Output, State
import requests
from access import get_db_connection

app = dash.Dash(__name__)

conn = get_db_connection()
cursor = conn.cursor()


def get_restaurant_names():
    try:
        response = requests.get("http://localhost:8000/restaurant_names/")
        response.raise_for_status()
        return response.json()
    except requests.exceptions.RequestException as e:
        print(f"Erreur lors de la récupération des noms de restaurants : {e}")
        return []


def save_response(nom, email, restaurant, note, type_pmr, type_visuel, type_auditif, commentaire):
    cursor.execute("SELECT id FROM utilisateur WHERE email = %s", (email,))
    user_id = cursor.fetchone()

    if user_id:
        user_id = user_id[0]
    else:
        cursor.execute("INSERT INTO utilisateur (nom_pseudo, email) VALUES (%s, %s)", (nom, email))
        user_id = cursor.lastrowid

    cursor.execute("SELECT id FROM restaurant WHERE nom = %s", (restaurant,))
    restaurant_id = cursor.fetchone()

    if not restaurant_id:
        return html.Div("Le restaurant n'existe pas dans la base de données.", style={'color': 'red'})

    restaurant_id = restaurant_id[0]

    cursor.execute(
        "INSERT INTO avis (utilisateur_id, restaurant_id, note, type_PMR, type_visuel, type_auditif, commentaire) "
        "VALUES (%s, %s, %s, %s, %s, %s, %s)",
        (user_id, restaurant_id, note, type_pmr, type_visuel, type_auditif, commentaire)
    )
    conn.commit()


app.layout = html.Div([
    html.H2("Questionnaire"),
    html.Label("Nom ou Pseudo"),
    html.Br(),
    dcc.Input(id='nom', type='text', placeholder='Votre nom ou pseudo'),
    html.Br(),
    html.Label("Adresse Mail"),
    html.Br(),
    dcc.Input(id='email', type='email', placeholder='Votre adresse mail'),
    html.Br(),
    html.Label("Nom du restaurant"),
    dcc.Dropdown(
        id='restaurant-dropdown',
        options=[{'label': name, 'value': name} for name in get_restaurant_names()],
        placeholder="Sélectionnez un restaurant"
    ),
    html.Br(),
    html.Label("Note sur 5"),
    dcc.RadioItems(
        id='note',
        options=[{'label': str(i), 'value': str(i)} for i in range(1, 6)],
        value='1'
    ),
    html.Br(),
    html.Label("Type d'accessibilité"),
    html.Br(),
    html.Label("PMR"),
    dcc.RadioItems(
        id='type_pmr',
        options=[
            {'label': 'Oui', 'value': 'oui'},
            {'label': 'Non', 'value': 'non'},
            {'label': 'Je ne sais pas', 'value': 'je_ne_sais_pas'}
        ],
        value='je_ne_sais_pas'
    ),
    html.Br(),
    html.Label("Adapté aux personnes en situation de handicap visuel"),
    dcc.RadioItems(
        id='type_visuel',
        options=[
            {'label': 'Oui', 'value': 'oui'},
            {'label': 'Non', 'value': 'non'},
            {'label': 'Je ne sais pas', 'value': 'je_ne_sais_pas'}
        ],
        value='je_ne_sais_pas'
    ),
    html.Br(),
    html.Label("Adapté aux personnes en situation de handicap auditif"),
    dcc.RadioItems(
        id='type_auditif',
        options=[
            {'label': 'Oui', 'value': 'oui'},
            {'label': 'Non', 'value': 'non'},
            {'label': 'Je ne sais pas', 'value': 'je_ne_sais_pas'}
        ],
        value='je_ne_sais_pas'
    ),
    html.Br(),
    html.Label("Ajout de commentaire (280 caractères)"),
    dcc.Textarea(
        id='commentaire',
        placeholder='Votre commentaire',
        maxLength=280,
        style={'width': '100%'}
    ),
    html.Br(),
    html.Div(
        [
            html.Button('Soumettre', id='submit', n_clicks=0),
            html.A(html.Button('Retour'), href='http://127.0.0.1:8050/', style={'margin-left': '10px'})
        ],
        style={'display': 'flex'}
    ),
    html.Div(id='output-state')
])


@callback(
    Output('output-state', 'children'),
    Input('submit', 'n_clicks'),
    [State('nom', 'value'),
     State('email', 'value'),
     State('restaurant-dropdown', 'value'),
     State('note', 'value'),
     State('type_pmr', 'value'),
     State('type_visuel', 'value'),
     State('type_auditif', 'value'),
     State('commentaire', 'value')]
)
def update_output(n_clicks, nom, email, restaurant, note, type_pmr, type_visuel, type_auditif, commentaire):
    if n_clicks > 0:
        if not nom or not email or not restaurant:
            return html.Div("Veuillez remplir tous les champs obligatoires.", style={'color': 'red'})

        save_response(nom, email, restaurant, note, type_pmr, type_visuel, type_auditif, commentaire)
        return html.Div("Merci pour votre réponse !", style={'color': 'green'})

    return ""


if __name__ == '__main__':
    app.run_server(debug=True)
