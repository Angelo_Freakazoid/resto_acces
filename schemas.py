from pydantic import BaseModel
from typing import List, Optional


class RestaurantBase(BaseModel):
    nom: str
    adresse: Optional[str]
    code_postal: Optional[str]
    ville: Optional[str]
    latitude: float
    longitude: float
    statut: str


class Restaurant(RestaurantBase):
    id: int


class TypeBase(BaseModel):
    type: str


class Type(TypeBase):
    id: int


class AvisBase(BaseModel):
    restaurant_id: int
    utilisateur_id: int
    note: int
    commentaire: Optional[str]
    type_PMR: str
    type_visuel: str
    type_auditif: str
    date_ajout: Optional[str]


class Avis(AvisBase):
    id: int


class RestaurantDetail(BaseModel):
    nom: str
    adresse: str
    note_moyenne: Optional[float]
    pmr_percentage: Optional[float]
    visuel_percentage: Optional[float]
    auditif_percentage: Optional[float]
    comments: List[dict]


class RestaurantLocation(BaseModel):
    id: int
    nom: str
    latitude: float
    longitude: float


class MessageResponse(BaseModel):
    message: str
