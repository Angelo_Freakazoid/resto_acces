DROP database if EXISTS restoacces;
CREATE database restoacces;
USE restoacces;


CREATE TABLE restaurant (
  id INT AUTO_INCREMENT PRIMARY KEY,
  nom VARCHAR(255) UNIQUE,
  adresse VARCHAR(255),
  code_postal CHAR(5),
  ville VARCHAR(255),
  latitude DECIMAL(9,6) NOT NULL,
  longitude DECIMAL(9,6) NOT NULL,
  statut ENUM('en attente de validation', 'validé') DEFAULT 'en attente de validation'
);

CREATE TABLE types (
  id INT PRIMARY KEY AUTO_INCREMENT,
  type VARCHAR(255) DEFAULT 'Autre'
);

CREATE TABLE types_restaurant (
  type_id INT,
  restaurant_id INT,
  FOREIGN KEY (type_id) REFERENCES types(id),
  FOREIGN KEY (restaurant_id) REFERENCES restaurant(id)
);

CREATE TABLE utilisateur (
  id INT AUTO_INCREMENT PRIMARY KEY,
  email VARCHAR(255),
  nom_pseudo VARCHAR(255) UNIQUE
);

CREATE TABLE avis (
  id INT AUTO_INCREMENT PRIMARY KEY,
  restaurant_id INT,
  utilisateur_id INT,
  note INT,
  commentaire TEXT,
  type_PMR ENUM('oui', 'non', 'je ne sais pas') DEFAULT 'je ne sais pas',
  type_visuel ENUM('oui', 'non', 'je ne sais pas') DEFAULT 'je ne sais pas',
  type_auditif ENUM('oui', 'non', 'je ne sais pas') DEFAULT 'je ne sais pas',
  date_ajout DATETIME,
  FOREIGN KEY (restaurant_id) REFERENCES restaurant(id),
  FOREIGN KEY (utilisateur_id) REFERENCES utilisateur(id)
);

