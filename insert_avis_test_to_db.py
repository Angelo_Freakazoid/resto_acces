import pandas as pd
from access import get_db_connection

csv_file_path = 'MOCK_DATA.csv'
df = pd.read_csv(csv_file_path)

conn = get_db_connection()
cursor = conn.cursor()

user_ids = {}
for index, row in df.iterrows():
    cursor.execute("""
        INSERT INTO utilisateur (nom_pseudo, email)
        VALUES (%s, %s)
        ON DUPLICATE KEY UPDATE email = VALUES(email)
    """, (row['nom'], row['email']))
    user_ids[row['email']] = cursor.lastrowid if cursor.lastrowid != 0 else cursor.fetchone()[0]

for index, row in df.iterrows():
    user_id = user_ids[row['email']]
    resto_id = row['resto']
    cursor.execute("""
            INSERT INTO avis (type_PMR, type_visuel, type_auditif, note, commentaire, utilisateur_id, restaurant_id)
            VALUES (%s, %s, %s, %s, %s, %s, %s)
        """, (
    row['type_PMR'], row['type_visuel'], row['type_auditif'], row['note'], row['commentaire'], user_id, resto_id))

conn.commit()
cursor.close()
conn.close()
