#!/bin/bash

# Créez un environnement virtuel
python3 -m venv venv

# Activez l'environnement virtuel
source venv/bin/activate

# Installez les dépendances
pip install -r requirements.txt

# Exécutez les script MySQL
sudo mysql < init.sql
sudo mysql < access.sql


# Exécutez les scripts Python pour insérer les données
python Insert_list_resto_to_db.py
python insert_avis_test_to_db.py

# Démarre l'application FastAPI avec uvicorn
uvicorn main:app --reload &

# Démarrez l'application Dash
python app_dash.py

# Désactivez l'environnement virtuel
deactivate
