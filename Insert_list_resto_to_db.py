from access import get_db_connection
import pandas as pd

file_path = 'restaurants_toulouse.csv'
data = pd.read_csv(file_path)

data[['Code_postal', 'Ville']] = data['Code_postal_Ville_restaurant'].str.split(' ', n=1, expand=True)
data[['Latitude', 'Longitude']] = data['Geopoint_restaurant'].str.split(',', expand=True)
data['Latitude'] = pd.to_numeric(data['Latitude'], errors='coerce')
data['Longitude'] = pd.to_numeric(data['Longitude'], errors='coerce')
data.drop(['Code_postal_Ville_restaurant', 'Geopoint_restaurant'], axis=1, inplace=True)

connection = get_db_connection()

cursor = connection.cursor()

insert_restaurant_query = """
INSERT INTO restaurant (nom, adresse, code_postal, ville, latitude, longitude, statut)
VALUES (%s, %s, %s, %s, %s, %s, 'validé')
"""

insert_type_query = "INSERT INTO types (type) VALUES (%s) ON DUPLICATE KEY UPDATE type = type"

select_type_id_query = "SELECT id FROM types WHERE type = %s"

insert_type_restaurant_query = "INSERT INTO types_restaurant (type_id, restaurant_id) VALUES (%s, %s)"

type_ids = {}

for index, row in data.iterrows():
    cursor.execute(insert_restaurant_query, (
        row['Nom_restaurant'],
        row['Adresse_restaurant'],
        row['Code_postal'],
        row['Ville'],
        row['Latitude'],
        row['Longitude']
    ))
    restaurant_id = cursor.lastrowid

    types = row['Type_restaurant'].split(', ')
    for type_restaurant in types:
        if type_restaurant not in type_ids:
            cursor.execute(insert_type_query, (type_restaurant,))
            cursor.execute(select_type_id_query, (type_restaurant,))
            type_id = cursor.fetchone()[0]
            type_ids[type_restaurant] = type_id
        else:
            type_id = type_ids[type_restaurant]

        cursor.execute(insert_type_restaurant_query, (type_id, restaurant_id))

connection.commit()

cursor.close()
connection.close()
